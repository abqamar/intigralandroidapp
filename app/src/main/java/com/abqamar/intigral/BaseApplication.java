
package com.abqamar.intigral;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;


import com.abqamar.intigral.utils.PrefUtil;

import java.util.Locale;

public class BaseApplication {

    private Application app = null;
    private static BaseApplication instance;

    public void initialize(Application application) {
        app = application;
    }

    public static BaseApplication getInstance() {

        if (instance == null) {
            instance = new BaseApplication();
        }

        return instance;
    }

    public Application getApplication() {
        return app;
    }

    public Context getAppContext() {
        Context ctx = null;

        if (app != null) {
            ctx = app.getApplicationContext();
        }

        return ctx;
    }

    public Resources getResources() {
        Resources res = null;

        Context ctx = getAppContext();

        if (ctx != null) {
            res = ctx.getResources();
        }

        return res;
    }

    public void setCurrentLocale(String lang) {

        String language = PrefUtil.getFromPrefs(getAppContext(), "lang", null);

        if (language == null) {
            lang = "en";
        }

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        Resources res = getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());

        PrefUtil.saveToPrefs(getAppContext(), "lang", lang);

    }

    public String getCurrentLocale() {
        Resources res = getResources();
        Locale locale = res.getConfiguration().locale;

        return locale.getLanguage();

    }
}
