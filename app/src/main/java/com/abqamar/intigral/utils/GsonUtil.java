
package com.abqamar.intigral.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

public class GsonUtil {

    // shared method for json coversion
    public static JsonObject getJsonObjectFromObject(final Object obj) {

        Gson gson = new Gson();
        String modelString = gson.toJson(obj);
        JsonParser parser = new JsonParser();
        JsonObject model = (JsonObject) parser.parse(modelString);

        return model;
    }

    public static String getEncodedStringFromObject(final Object obj) {
        String urlEnocoder = "";
        Gson gson = new Gson();
        String modelString = gson.toJson(obj);
        JsonParser parser = new JsonParser();
        JsonObject model = (JsonObject) parser.parse(modelString);
        try {
            urlEnocoder = URLEncoder.encode(GsonUtil.getJsonObjectFromObject(model).toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return urlEnocoder;
    }

    public static <T> Object getObjectFromJsonObject(final Object data, Class<T> classofT) {

        T obj = null;

        if (data instanceof JsonObject) {
            JsonObject json = (JsonObject) data;
            Gson gson = new Gson();
            obj = gson.fromJson(json, classofT);
        }
        
        return obj;
    }

    public static <T> Object getObjectFromJsonString(String jsonStr, Class<T> classofT){
        T obj = null;

        if (jsonStr != null && !jsonStr.equalsIgnoreCase("")) {
            try {

            Gson gson = new Gson();
            JsonElement element = gson.fromJson(jsonStr, JsonElement.class);
            JsonObject jsonObj = element.getAsJsonObject();
            obj = gson.fromJson(jsonObj, classofT);

            } catch (Exception e){
            }

        }

        return obj;
    }

    public static boolean hasKeyInJsonString(String jsonStr, String key) {
        if (jsonStr != null && !jsonStr.equalsIgnoreCase("")) {
            try {

                Gson gson = new Gson();
                JsonElement element = gson.fromJson(jsonStr, JsonElement.class);
                JsonObject jsonObj = element.getAsJsonObject();
                if (jsonObj.has(key)) {
                    return true;
                }
                return false;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public static final <T> ArrayList<T> getListForJsonKey(final Class<T[]> clazz, final String jsonStr, final String key) {
        if (jsonStr != null && !jsonStr.equalsIgnoreCase("")) {
            try {

                Gson gson = new Gson();
                JsonElement element = gson.fromJson(jsonStr, JsonElement.class);
                JsonObject jsonObj = element.getAsJsonObject();
                if (jsonObj.has(key)) {
                    final T[] jsonToObject = gson.fromJson(jsonObj.getAsJsonArray(key), clazz);
                    return new ArrayList<T>(Arrays.asList(jsonToObject));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
        return null;
    }

    public static <T> Object getObjectForJsonKey(String jsonStr, String key, Class<T> classofT) {

        T obj = null;
        if (jsonStr != null && !jsonStr.equalsIgnoreCase("")) {
            try {

                Gson gson = new Gson();

                JsonElement element = gson.fromJson(jsonStr, JsonElement.class);
                JsonObject jsonObj = element.getAsJsonObject();
                if (jsonObj.has(key)) {
                    obj = gson.fromJson(jsonObj.getAsJsonArray(key), classofT);
                    return obj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return obj;
            }
        } else {
            return obj;
        }
        return obj;
    }


}
