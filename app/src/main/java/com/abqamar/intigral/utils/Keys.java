package com.abqamar.intigral.utils;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class Keys {

    public static final String api_key = "api_key";
    public static final String language = "language";
    public static final String page = "page";
    public static final String sort_by = "sort_by";


}
