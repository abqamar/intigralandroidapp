package com.abqamar.intigral.utils;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class Constants {

    public static final String API_KEY = "114fe6670282f6a632638661e5e86dee";

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_URL_ATTACHMENT = "https://image.tmdb.org/t/p/w500";

    //https://api.themoviedb.org/3/discover/movie?api_key=&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1

    public static final String URL_DISCOVER = BASE_URL + "discover/movie";



    public static final int RESPONSE_POPULAR = 1;
    public static final int RESPONSE_TOP_RATED = 2;
    public static final int RESPONSE_REVENUE = 3;

}
