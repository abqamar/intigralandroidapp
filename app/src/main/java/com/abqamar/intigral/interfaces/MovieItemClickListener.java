package com.abqamar.intigral.interfaces;

import android.widget.ImageView;

import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesListBean;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public interface MovieItemClickListener {
    void onMovieItemClick(int pos, DiscoverMoviesListBean movieItem, ImageView shareImageView);
}
