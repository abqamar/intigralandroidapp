package com.abqamar.intigral.network;


import java.util.Map;

public class RequestData {
    
    public static final int GET = 1;
    public static final int POST = 2;
    public static final int JSON = 3;
    public static final int POST_JSON = 6;
    public static final int MULTIPART = 4;
    public static final int POST_JSON_BEAN = 5;
    public static final int GET_REGISTRATION = 7;
    
    public int method;
    
    public Map<String, ?> params;

    public Object sharedModel;
    
    public int serviceType;
    
    public ResponseListener responseListener;

    public OnErrorListener onErrorListener;
    
    public String url;

    public String token;
}
