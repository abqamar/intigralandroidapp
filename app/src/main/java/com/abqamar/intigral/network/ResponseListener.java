package com.abqamar.intigral.network;

import com.koushikdutta.ion.Response;

public interface ResponseListener {

    public void onResponseListener(int serviceType, Exception e, Response<String> response);
}
