package com.abqamar.intigral.network;

import com.koushikdutta.ion.Response;

/**
 * Created by akhmas.ibtesam on 3/2/2015.
 */
public interface OnErrorListener {

    public void onErrorListener(int serviceType, Exception e, Response<String> response);
}
