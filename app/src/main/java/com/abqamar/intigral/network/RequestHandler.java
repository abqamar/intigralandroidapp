
package com.abqamar.intigral.network;

import android.util.Log;

import com.abqamar.intigral.BaseApplication;
import com.abqamar.intigral.utils.GsonUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestHandler {

    private static String TAG = "AMAF REQUEST";
    public static final String ANDROID_TOKEN = "KIzsdNDYwVTe9EWo1qxdQ1lIg1vrYHUWufzhTQ";

    @SuppressWarnings("unchecked")
    public static void sendWebserviceRequest(RequestData data) {

        //TODO PERMISSION

        switch (data.method) {
            case RequestData.GET:
                sendGetRequest(data.serviceType, (Map<String, String>) data.params,
                        data.responseListener, data.onErrorListener, data.url, data.token);
                break;

            case RequestData.POST:
                sendPostRequest(data.serviceType,
                        (Map<String, String>) data.params, data.responseListener,
                        data.url, data.token);
                break;

            case RequestData.POST_JSON:
                sendPostRequestHeader(data.serviceType,
                        (Map<String, String>) data.params, data.responseListener,
                        data.url, data.token);
                break;

            case RequestData.JSON:
                sendJsonRequest(data.serviceType,
                        (Map<String, String>) data.params, data.responseListener,
                        data.url, data.token);
                break;

            case RequestData.POST_JSON_BEAN:
                sendJsonRequestFromBean(data.serviceType, data.responseListener, data.onErrorListener, data, data.token);
                break;

            case RequestData.GET_REGISTRATION:
                sendGetRequestForRegistration(data.serviceType, (Map<String, String>) data.params,
                        data.responseListener, data.onErrorListener, data.url);
                break;

            default:
                break;
        }

    }

    private static void sendJsonRequestFromBean(
            final int serviceType,
            final ResponseListener responseListener,
            final OnErrorListener onErrorListener,
            final RequestData data, String loginToken) {

        Ion.with(BaseApplication.getInstance().getApplication()).load(data.url)
                .setJsonObjectBody(GsonUtil.getJsonObjectFromObject(data.sharedModel)).asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {


                        if (result == null || result.getResult() == null || result.getResult().contains("\"errors\":")) {
                            onErrorListener.onErrorListener(serviceType, e, result);
                        } else {
                            responseListener.onResponseListener(serviceType, e,
                                    result);
                        }
                    }
                });

    }

    private static void sendJsonRequest(final int serviceType,
                                        Map<String, String> params,
                                        final ResponseListener responseListener, String url, String logintoken) {


        Ion.with(BaseApplication.getInstance().getApplication()).load(url)
                .setJsonObjectBody(getJsonFromMap(params)).asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        responseListener.onResponseListener(serviceType, e,
                                result);
                    }
                });


    }



    private static void sendGetRequest(final int serviceType,
                                       Map<String, String> params,
                                       final ResponseListener responseListener,
                                       final OnErrorListener onErrorListener,
                                       String url, String loginToken) {

        if(loginToken == null ){
            loginToken = "";
        }

        Ion.with(BaseApplication.getInstance().getApplication())
                .load(getFinalUrl(url, params))
                .asString().withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
//                        String resultStr = result.getResult().toString();
                        if (result == null || result.getResult() == null || result.getResult().contains("\"errors\":")) {
                            onErrorListener.onErrorListener(serviceType, e, result);
                        } else {
                            responseListener.onResponseListener(serviceType, e,result);
                        }
                    }
                });

    }

    private static void sendGetRequestForRegistration(final int serviceType,
                                       Map<String, String> params,
                                       final ResponseListener responseListener,
                                       final OnErrorListener onErrorListener,
                                       String url) {

        Ion.with(BaseApplication.getInstance().getApplication())
                .load(getFinalUrl(url, params))
                .asString().withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
//                        String resultStr = result.getResult().toString();
                        if (result == null || result.getResult() == null || result.getResult().contains("\"errors\":")) {
                            onErrorListener.onErrorListener(serviceType, e, result);
                        } else {
                            responseListener.onResponseListener(serviceType, e,result);
                        }
                    }
                });

    }

    public static void sendPostRequest(final int serviceType,
                                       Map<String, String> params,
                                       final ResponseListener responseListener,
                                       String url, String loginToken) {

        Ion.with(BaseApplication.getInstance().getApplication()).load(url)
                .setBodyParameters(getFinalMap(params)).asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        responseListener.onResponseListener(serviceType, e,
                                result);
                    }
                });
    }

    public static void sendPostRequestHeader(final int serviceType,
                                             Map<String, String> params,
                                             final ResponseListener responseListener,
                                             String url , String loginToken) {

        String jsonObject = params.get("inputParameters");

        Ion.with(BaseApplication.getInstance().getApplication()).load(url)
                .setJsonObjectBody( new Gson().fromJson(jsonObject, JsonObject.class))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        Log.e("header -----------", result.getHeaders().toHeaderString());
                        responseListener.onResponseListener(serviceType, e,
                                result);
                    }
                });
    }

    public static void sendMultipartRequest(final int serviceType, String key,
                                            File file, final ResponseListener responseListener, String url) {

        Ion.with(BaseApplication.getInstance().getApplication()).load(url)
                .setMultipartFile(key, file).asString().withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        responseListener.onResponseListener(serviceType, e,
                                result);
                    }
                });


    }

    public static void sendDynamicMultipartRequest(final int serviceType,
                                                   String key,
                                                   ArrayList<String> filePath,
                                                   final ResponseListener responseListener,
                                                   final OnErrorListener onErrorListener,
                                                   String url,
                                                   Map<String, String> params, String loginToken) {


        //MultipartBodyBuilder<?> multiPartBuilder = Ion.with(BaseApplication.getInstance().getApplication()).load(url);
        ArrayList<Part> filesParts = new ArrayList<>();
        for (String path : filePath) {
            filesParts.add(new FilePart("File", new File(path)));
        }


        /*for (int i =0;i<filePath.size();i++){
            //multiPartBuilder.setMultipartFile("File"+i,new File(filePath.get(i)));
            Ion.with(BaseApplication.getInstance().getApplication()).load(url).setMultipartFile("", new File(filePath.get(i)));
        }*/

        for (Map.Entry<String, String> param : params.entrySet()) {
            //multiPartBuilder.setMultipartParameter(param.getKey(),param.getValue());
            Ion.with(BaseApplication.getInstance().getApplication()).load(url).setMultipartParameter(param.getKey(), param.getValue());
        }
        //Ion.with(BaseApplication.getInstance().getApplication()).load(url).uploadProgressDialog();

        Ion.with(BaseApplication.getInstance().getApplication()).load(url)
                .addMultipartParts(filesParts).asString().withResponse().setCallback(new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                if (result == null || result.getResult() == null || result.getResult().contains("\"errors\":")) {
                    onErrorListener.onErrorListener(serviceType, e, result);
                } else {
                    responseListener.onResponseListener(serviceType, e,
                            result);
                }
            }
        });
    }

    private static String getFinalUrl(String url, Map<String, String> params) {

        if (params != null) {

            boolean flag = true;
            url = url + "?";
            for (Map.Entry<String, String> param : params.entrySet()) {
                if (flag) {
                    url = url + param.getKey() + "=" + param.getValue();
                    flag = false;
                } else {
                    url = url + "&" + param.getKey() + "=" + param.getValue();
                }
            }

        }


        return url;
    }

    private static JsonObject getJsonFromMap(Map<String, String> map) {
        JsonObject json = new JsonObject();

        for (Map.Entry<String, String> param : map.entrySet()) {

            json.addProperty(param.getKey(), param.getValue());

        }
        return json;

    }

    private static Map<String, List<String>> getFinalMap(Map<String, String> map) {

        Map<String, List<String>> finalMap = new HashMap<String, List<String>>();
        List<String> list;

        for (Map.Entry<String, String> param : map.entrySet()) {
            list = new ArrayList<String>();

            list.add(param.getValue());

            finalMap.put(param.getKey(), list);
        }

        return finalMap;

    }

}
