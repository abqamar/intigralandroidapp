package com.abqamar.intigral.network;

/**
 * Created by akhmas.ibtesam on 6/4/2015.
 */
public interface RestClientResponseListener {

    public void onRestClientResponseListener(int serviceType, String response);
}
