package com.abqamar.intigral.network;

/**
 * Created by akhmas.ibtesam on 6/4/2015.
 */
public interface RestClientErrorListener {

    public void onRestClientErrorListener(int serviceType, String response);
}
