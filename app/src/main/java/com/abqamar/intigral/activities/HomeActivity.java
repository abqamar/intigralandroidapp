package com.abqamar.intigral.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.abqamar.intigral.IntigralBaseActivity;
import com.abqamar.intigral.R;
import com.abqamar.intigral.fragments.HomeFragment;

public class HomeActivity extends IntigralBaseActivity {
    
    private Handler mHandler;
    public static String CURRENT_TAG = "Home";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mHandler = new Handler();

        //setupToolbar
        setUpToolbar();

        // set toolbar title
        setToolbarTitle("Intigral");

        initViews();
        initListeners();


    }

    private void initViews(){

    }

    private void initListeners(){
        // load first fragment of app
        loadHomeFragment();
    }

    /***
     * Returns respected fragment that user
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item


        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = HomeFragment.getInstance();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
    }
}
