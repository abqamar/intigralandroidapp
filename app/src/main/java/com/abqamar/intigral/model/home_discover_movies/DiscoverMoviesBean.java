package com.abqamar.intigral.model.home_discover_movies;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class DiscoverMoviesBean implements Serializable {

    private int page;
    private int total_results;
    private int total_pages;
    private ArrayList<DiscoverMoviesListBean> results = new ArrayList<DiscoverMoviesListBean>();

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<DiscoverMoviesListBean> getResults() {
        return results;
    }

    public void setResults(ArrayList<DiscoverMoviesListBean> results) {
        this.results = results;
    }
}
