package com.abqamar.intigral;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by Muhammad Abdul Basit Qamar on 02/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class IntigralApplication extends Application {

    private static IntigralApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        BaseApplication.getInstance().initialize(this);
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }

    public static IntigralApplication getInstance() {
        return sInstance;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //changeLocale();
    }
}
