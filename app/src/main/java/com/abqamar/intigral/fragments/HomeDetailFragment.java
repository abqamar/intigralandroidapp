package com.abqamar.intigral.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.abqamar.intigral.IntigralBaseActivity;
import com.abqamar.intigral.IntigralBaseFragment;
import com.abqamar.intigral.R;
import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesBean;
import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesListBean;
import com.abqamar.intigral.utils.Constants;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class HomeDetailFragment extends IntigralBaseFragment {

    private View baseView;
    private ImageView imgMovie;
    private String mTransitionName;

    private DiscoverMoviesListBean moviesListItem;

    // fetching object as string from getArguments for movieItem
    private String moviesListItemStringData;

    private RatingBar ratingBar;
    private TextView ratingValue, movieName, movieDescription;

    public static final String KEY_MOVIE_ITEM = "movie_item";
    private static final String KEY_TRANSITION_NAME = "transition_name";

    public static HomeDetailFragment newInstance(DiscoverMoviesListBean movieItem, String transitionName){
        HomeDetailFragment fragment =  new HomeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_MOVIE_ITEM, new Gson().toJson(movieItem));
        bundle.putString(KEY_TRANSITION_NAME, transitionName);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        setToolbarTitle("Details");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }

        mTransitionName = getArguments() != null ? getArguments().getString(KEY_TRANSITION_NAME) : null;
        moviesListItemStringData = getArguments() != null ? getArguments().getString(KEY_MOVIE_ITEM) : null;

        if(moviesListItemStringData != null)
            moviesListItem = (DiscoverMoviesListBean) new Gson().fromJson(moviesListItemStringData, DiscoverMoviesListBean.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.fragment_detail, container,false);

        imgMovie = (ImageView) baseView.findViewById(R.id.imgMovie);
        ratingBar = (RatingBar) baseView.findViewById(R.id.ratingBar);
        ratingValue = (TextView) baseView.findViewById(R.id.ratingValue);
        movieDescription = (TextView) baseView.findViewById(R.id.movieDescription);
        movieName = (TextView) baseView.findViewById(R.id.movieName);

        return baseView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgMovie.setTransitionName(mTransitionName);
        }

        Picasso.with(getContext())
                .load(Constants.BASE_URL_ATTACHMENT + moviesListItem.getPoster_path())
                .noFade()
                .into(imgMovie, new Callback() {
                    @Override
                    public void onSuccess() {
                        startPostponedEnterTransition();
                    }

                    @Override
                    public void onError() {
                        startPostponedEnterTransition();
                    }
                });

        ratingBar.setRating(moviesListItem.getVote_average()/2);
        ratingValue.setText(moviesListItem.getVote_average()+"/10");

        movieName.setText(moviesListItem.getTitle());
        movieDescription.setText(moviesListItem.getOverview());

    }
}
