package com.abqamar.intigral.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.abqamar.intigral.IntigralBaseFragment;
import com.abqamar.intigral.R;
import com.abqamar.intigral.activities.HomeActivity;
import com.abqamar.intigral.adapters.DiscoverMoviesRecyclerAdapter;
import com.abqamar.intigral.interfaces.MovieItemClickListener;
import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesBean;
import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesListBean;
import com.abqamar.intigral.network.OnErrorListener;
import com.abqamar.intigral.network.ResponseListener;
import com.abqamar.intigral.utils.Constants;
import com.abqamar.intigral.utils.EndlessScrollListener;
import com.abqamar.intigral.webservices.Webservices;
import com.google.gson.Gson;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class HomeFragment extends IntigralBaseFragment  implements ResponseListener, OnErrorListener, MovieItemClickListener {

    public static HomeFragment getInstance(){
        return new HomeFragment();
    }

    private View baseView;

    private RecyclerView popularRecyclerView, topRatedRecyclerView, revenueRecyclerView;
    private Webservices webservices;
    private boolean isViewLoaded = false;

    // Recyclerview Layout Mangers of different categories
    GridLayoutManager popularLayoutManager;
    GridLayoutManager topRatedLayoutManager;
    GridLayoutManager revenueLayoutManager;

    // Categories page counts
    private int pageCountPopular = 1;
    private int pageCountRevenue = 1;
    private int pageCountTopRated = 1;

    // RecyclerView adapters of different Categories
    DiscoverMoviesRecyclerAdapter popularAdapter;
    DiscoverMoviesRecyclerAdapter topRatedAdapter;
    DiscoverMoviesRecyclerAdapter revenueAdapter;

    // Sorting Parameters of different Categories
    public static final String POPULAR = "popularity.desc";
    public static final String REVENUE = "revenue.desc";
    public static final String TOP_RATED = "vote_average.desc";

    // Categories List
    private ArrayList<DiscoverMoviesListBean> populartArray = new ArrayList<DiscoverMoviesListBean>();
    private ArrayList<DiscoverMoviesListBean> topRatedArray = new ArrayList<DiscoverMoviesListBean>();
    private ArrayList<DiscoverMoviesListBean> revenueArray = new ArrayList<DiscoverMoviesListBean>();



    @Override
    public void onResume() {
        super.onResume();

        setToolbarTitle("Home");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(!isViewLoaded){
            baseView = inflater.inflate(R.layout.fragment_home, container, false);
            initViews();
            initObj();
            showMaterialWheel();
            callService(pageCountPopular, POPULAR, Constants.RESPONSE_POPULAR);
            callService(pageCountTopRated, TOP_RATED, Constants.RESPONSE_TOP_RATED);
            callService(pageCountRevenue, REVENUE, Constants.RESPONSE_REVENUE);
        }
        return baseView;
    }

    private void initViews(){
        popularLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        topRatedLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        revenueLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        popularRecyclerView = (RecyclerView) baseView.findViewById(R.id.rv_popular);
        topRatedRecyclerView = (RecyclerView) baseView.findViewById(R.id.rv_top_rated);
        revenueRecyclerView = (RecyclerView) baseView.findViewById(R.id.rv_revenue);
        popularRecyclerView.setLayoutManager(popularLayoutManager);
        topRatedRecyclerView.setLayoutManager(topRatedLayoutManager);
        revenueRecyclerView.setLayoutManager(revenueLayoutManager);
    }

    private void initObj(){
        webservices = new Webservices();
    }

    private void callService(int pageCount, String sortBy, int type){
        webservices.callGetDiscoverMovies(this,this,pageCount, sortBy, type);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        popularAdapter = new DiscoverMoviesRecyclerAdapter(getContext(), populartArray,this);
        popularRecyclerView.setAdapter(popularAdapter);

        topRatedAdapter = new DiscoverMoviesRecyclerAdapter(getContext(), topRatedArray,this);
        topRatedRecyclerView.setAdapter(topRatedAdapter);

        revenueAdapter = new DiscoverMoviesRecyclerAdapter(getContext(), revenueArray,this);
        revenueRecyclerView.setAdapter(revenueAdapter);

        popularRecyclerView.setOnScrollListener(new EndlessScrollListener(popularLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("paging = ", current_page+"");
                pageCountPopular = current_page;
                callService(pageCountPopular, POPULAR, Constants.RESPONSE_POPULAR);

            }
        });

        revenueRecyclerView.setOnScrollListener(new EndlessScrollListener(revenueLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("paging = ", current_page+"");
                pageCountRevenue = current_page;
                callService(pageCountRevenue, REVENUE,Constants.RESPONSE_REVENUE);
            }
        });

        topRatedRecyclerView.setOnScrollListener(new EndlessScrollListener(topRatedLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("paging = ", current_page+"");
                pageCountTopRated = current_page;
                callService(pageCountTopRated, TOP_RATED,Constants.RESPONSE_TOP_RATED);
            }
        });
        isViewLoaded = true;
    }

    @Override
    public void onResponseListener(int serviceType, Exception e, Response<String> response) {
        try{
            if(this != null){
                stopMaterialWheel();
                switch (serviceType){
                    case Constants.RESPONSE_POPULAR:

                        if(response != null && response.getResult() != null){
                            DiscoverMoviesBean responseModel = (DiscoverMoviesBean) new Gson().fromJson(response.getResult(), DiscoverMoviesBean.class);

                            if(responseModel != null && responseModel.getResults().size() > 0){

                                populartArray.addAll(responseModel.getResults());
                                popularAdapter.notifyDataSetChanged();
                            }

                        }

                        break;

                    case Constants.RESPONSE_TOP_RATED:

                        if(response != null && response.getResult() != null){
                            DiscoverMoviesBean responseModel = (DiscoverMoviesBean) new Gson().fromJson(response.getResult(), DiscoverMoviesBean.class);

                            if(responseModel != null && responseModel.getResults().size() > 0){

                                topRatedArray.addAll(responseModel.getResults());
                                topRatedAdapter.notifyDataSetChanged();
                            }

                        }

                        break;

                    case Constants.RESPONSE_REVENUE:

                        if(response != null && response.getResult() != null){
                            DiscoverMoviesBean responseModel = (DiscoverMoviesBean) new Gson().fromJson(response.getResult(), DiscoverMoviesBean.class);

                            if(responseModel != null && responseModel.getResults().size() > 0){

                                revenueArray.addAll(responseModel.getResults());
                                revenueAdapter.notifyDataSetChanged();
                            }

                        }

                        break;
                }
            }
        }catch (Exception ex){
            stopMaterialWheel();
        }
    }

    @Override
    public void onErrorListener(int serviceType, Exception e, Response<String> response) {
        try{
            if(this != null){
                stopMaterialWheel();
                switch (serviceType){
                    case Constants.RESPONSE_POPULAR:
                        break;
                    case Constants.RESPONSE_TOP_RATED:
                        break;
                    case Constants.RESPONSE_REVENUE:
                        break;
                }
            }
        }catch (Exception ex){
            stopMaterialWheel();
        }
    }

    @Override
    public void onMovieItemClick(int pos, DiscoverMoviesListBean movieItem, ImageView shareImageView) {
        Fragment homeDetailFragment = HomeDetailFragment.newInstance(movieItem, ViewCompat.getTransitionName(shareImageView));
        getFragmentManager()
                .beginTransaction()
                .addSharedElement(shareImageView, ViewCompat.getTransitionName(shareImageView))
                .addToBackStack(null)
                .replace(R.id.frame, homeDetailFragment)
                .commit();
    }
}
