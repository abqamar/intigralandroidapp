
package com.abqamar.intigral.webservices;

import android.app.Activity;
import android.content.Context;

import com.abqamar.intigral.network.RestClientErrorListener;
import com.abqamar.intigral.network.RestClientResponseListener;
import com.koushikdutta.ion.Response;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

public class RestClient {

    private ArrayList<NameValuePair> params;

    private ArrayList<NameValuePair> headers;

    private String url;

    private int responseCode;

    private String message;

    private String response;

    private String responseEncoded;

    private boolean enableEncoding = false;

    private RestClientResponseListener responseListener;

    private RestClientErrorListener onErrorListener;

    private int serviceId;

    private Response<String> result;

    private Context context;

    public String getResponse() {
        return response;
    }

    public boolean isEnableEncoding() {
        return enableEncoding;
    }

    public void setEnableEncoding(boolean enableEncoding) {
        this.enableEncoding = enableEncoding;
    }

    public String getResponseEncoded() {
        return responseEncoded;
    }

    public void setResponseEncoded(String responseEncoded) {
        this.responseEncoded = responseEncoded;
    }

    public String getErrorMessage() {
        return message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public RestClient(String url) {
        this.url = url;
        params = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
    }

    public void AddParam(String name, String value) {
        params.add(new BasicNameValuePair(name, value));
    }

    public void AddHeader(String name, String value) {
        headers.add(new BasicNameValuePair(name, value));
    }

    public void AddListeners(RestClientResponseListener responseListener,RestClientErrorListener onErrorListener){
        this.responseListener = responseListener;
        this.onErrorListener = onErrorListener;
    }

    public void ServiceType(int serviceId){
        this.serviceId = serviceId;
    }

    public void setContext(Context context){
        this.context = context;
    }

    public void Execute(RequestMethod method) throws Exception {
        switch (method) {
            case GET: {
                // add parameters
                String combinedParams = "";
                if (!params.isEmpty()) {
                    combinedParams += "?";
                    for (NameValuePair p : params) {
                        String paramString = p.getName() + "="
                                + URLEncoder.encode(p.getValue(), "UTF-8");

                        if (combinedParams.length() > 1) {
                            combinedParams += "&" + paramString;
                        } else {
                            combinedParams += paramString;
                        }
                    }
                }

                HttpGet request = new HttpGet(url + combinedParams);

                // add headers
                for (NameValuePair h : headers) {
                    request.addHeader(h.getName(), h.getValue());
                }

                executeRequest(request, url);
                break;
            }
            case POST: {
                HttpPost request = new HttpPost(url);

                // add headers
                for (NameValuePair h : headers) {
                    request.addHeader(h.getName(), h.getValue());
                }

                if (!params.isEmpty()) {
                    request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                }

                executeRequest(request, url);
                break;
            }
        }
    }

    private void executeRequest(HttpUriRequest request, String url) {
        HttpClient client = new DefaultHttpClient();

        HttpResponse httpResponse;

        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {

                InputStream instream = entity.getContent();

                if (enableEncoding) {
                    responseEncoded = IOUtils.toString(instream, "windows-1256");
                    response = responseEncoded;
                } else {
                    response = convertStreamToString(instream);

                }

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (response == null || response == null || response.contains("\"errors\":")){
                            onErrorListener.onRestClientErrorListener(serviceId,response);
                        }else{
                            responseListener.onRestClientResponseListener(serviceId,response);
                        }

                    }
                });

                // Closing the input stream will trigger connection release
                instream.close();
            }

        } catch (ClientProtocolException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public enum RequestMethod {
        GET, POST
    }
}
