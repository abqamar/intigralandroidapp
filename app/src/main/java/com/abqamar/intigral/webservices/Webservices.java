package com.abqamar.intigral.webservices;

import com.abqamar.intigral.network.OnErrorListener;
import com.abqamar.intigral.network.RequestData;
import com.abqamar.intigral.network.RequestHandler;
import com.abqamar.intigral.network.ResponseListener;
import com.abqamar.intigral.utils.Constants;
import com.abqamar.intigral.utils.Keys;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by akhmas.ibtesam on 2/16/2015.
 */
public class Webservices {


    public void callGetDiscoverMovies(ResponseListener listener, OnErrorListener onErrorListener, int pageCount, String sortBy, int type) {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put(Keys.api_key, Constants.API_KEY);
        map.put(Keys.language, "en-US");
        map.put(Keys.sort_by, sortBy);
        map.put(Keys.page, pageCount+"");
        RequestData data = new RequestData();
        data.url = Constants.URL_DISCOVER;
        data.serviceType = type;
        data.params = map;
        data.sharedModel = null;
        data.responseListener = listener;
        data.onErrorListener = onErrorListener;
        data.method = RequestData.GET;

        RequestHandler.sendWebserviceRequest(data);

    }

}
