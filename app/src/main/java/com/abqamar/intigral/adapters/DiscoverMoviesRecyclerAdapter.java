package com.abqamar.intigral.adapters;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abqamar.intigral.R;
import com.abqamar.intigral.interfaces.MovieItemClickListener;
import com.abqamar.intigral.model.home_discover_movies.DiscoverMoviesListBean;
import com.abqamar.intigral.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class DiscoverMoviesRecyclerAdapter extends RecyclerView.Adapter<DiscoverMoviesRecyclerAdapter.ListItemHolder> {

    private Context mContext;
    private ArrayList<DiscoverMoviesListBean> mList = new ArrayList<>();
    private MovieItemClickListener itemClickListener;

    public DiscoverMoviesRecyclerAdapter(Context context, ArrayList<DiscoverMoviesListBean> list, MovieItemClickListener movieItemClickListener){
        mContext = context;
        mList = list;
        itemClickListener = movieItemClickListener;
    }

    @Override
    public ListItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(mContext).inflate(R.layout.item_discover_movies,parent, false);
        return new ListItemHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final ListItemHolder holder, int position) {
        final DiscoverMoviesListBean data = (DiscoverMoviesListBean) mList.get(position);

        if(data != null){
            //Picasso.with(mContext).load(Constants.BASE_URL_ATTACHMENT+data.getPoster_path()).fit().into(holder.imgMovie);

            Picasso.with(holder.itemView.getContext())
                    .load(Constants.BASE_URL_ATTACHMENT+data.getPoster_path())
                    .into(holder.imgMovie);

            ViewCompat.setTransitionName(holder.imgMovie, data.getPoster_path());

            holder.imgMovie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onMovieItemClick(holder.getAdapterPosition(), data, holder.imgMovie);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ListItemHolder extends RecyclerView.ViewHolder {

        private ImageView imgMovie;

        public ListItemHolder(View itemView) {
            super(itemView);
            imgMovie = (ImageView) itemView.findViewById(R.id.imgMovie);
        }

    }
}
