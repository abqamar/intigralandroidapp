package com.abqamar.intigral;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.abqamar.intigral.webservices.Webservices;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class IntigralBaseFragment extends Fragment {
    public IntigralBaseActivity activity;
    protected Webservices webservices;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Fragment class", getClass() + "");
        activity = (IntigralBaseActivity) getActivity();
        initObjs();
    }

    public void setToolbarTitle(String title){
        activity.setToolbarTitle(title);
    }

    private void initObjs() {
        webservices = new Webservices();
    }

    public void showMaterialWheel() {
        activity.showMaterialWheel();
    }

    public boolean isMaterialWheelShowing() {
        return activity.isMaterialWheelShowing();
    }

    public void stopMaterialWheel() {
        activity.stopMaterialWheel();
    }

    public void initProgessWheel(View baseView) {
        activity.initProgessWheel(baseView
        );
    }

    public void showProgessWheel() {
        activity.showProgessWheel();
    }

    public void showProgessWheel(View baseView) {
        activity.showProgessWheel(baseView);
    }

    public boolean isProgressWheelShowing() {
        return activity.isProgressWheelShowing();
    }

    public void stopProgressWheel() {
        activity.stopProgressWheel();
    }
}
