package com.abqamar.intigral;

import android.app.Dialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.pnikosis.materialishprogress.ProgressWheel;

/**
 * Created by Muhammad Abdul Basit Qamar on 03/10/2017.
 * http://abqamar.irozon.com
 * Emitac Enterprise Solutions
 */

public class IntigralBaseActivity extends BaseToolbarActivity {
    private ProgressWheel wheel;
    private View dialogView;
    private Dialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initProgessWheel();

        //setupCustomActionbar(R.layout.actionbar_header);
        //setParent(true);
    }


    private void initMaterialWheel() {
        dialogView = getLayoutInflater().inflate(R.layout.progressbar_circular, null);
        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setCancelable(false);
        progressDialog.setContentView(dialogView);

    }

    public void showMaterialWheel() {


        if (null != progressDialog && isMaterialWheelShowing()) {
            stopMaterialWheel();
            progressDialog = null;
        }
        initMaterialWheel();

        progressDialog.show();
        clearParentsBackgrounds(dialogView);
    }

    public boolean isMaterialWheelShowing() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public void stopMaterialWheel() {
        if (this != null) {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    protected void addKeyboardObserverToLayout(final View mainView) {
        mainView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                mainView.getWindowVisibleDisplayFrame(r);
                int heightDiff = mainView.getRootView().getHeight() - (r.bottom - r.top);

                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    //ok now we know the keyboard is up...


                } else {
                    //ok now we know the keyboard is down...


                }
            }
        });
    }

    private void clearParentsBackgrounds(View view) {
        while (view != null) {
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View) parent;
                view.setBackgroundResource(android.graphics.Color.TRANSPARENT);
            } else {
                view = null;
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (wheel != null) {
            if (wheel.isSpinning()) {
                wheel.stopSpinning();
            }
            wheel = null;
        }
        if (progressDialog != null) {
            if (isMaterialWheelShowing()) {
                stopMaterialWheel();
            }
            progressDialog = null;
        }


        super.onDestroy();
    }

    public void showProgessWheel() {
        if (null != wheel && isProgressWheelShowing()) {
            stopProgressWheel();
            wheel = null;
        }

        initProgessWheel();
        wheel.spin();
    }

    public void showProgessWheel(View baseView) {
        if (null != wheel && isProgressWheelShowing()) {
            stopProgressWheel();
            wheel = null;
        }

        initProgessWheel(baseView);
        wheel.spin();
    }

    public boolean isProgressWheelShowing() {
        if (wheel != null) {
            if (wheel.isSpinning()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public void stopProgressWheel() {
        if (this != null) {
            if (wheel != null) {
                wheel.stopSpinning();
                wheel = null;
            }
        }
    }

    public void initProgessWheel() {
        wheel = (ProgressWheel) findViewById(R.id.progress_wheel);
    }

    public void initProgessWheel(View baseView) {
        wheel = (ProgressWheel) baseView.findViewById(R.id.progress_wheel);
    }
}
